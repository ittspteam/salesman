#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{		
	TEST_CLASS(UnitTestObject)
	{
	public:
		
		TEST_METHOD(Object_base)
		{
			tsp::Object a;
			Assert::IsNull(a.data);
		}


		TEST_METHOD(Object_initial)
		{
			tsp::Object a(2);
			Assert::IsTrue((a.datasize==2)&&(a.data!=nullptr));
		}

		TEST_METHOD(Object_copy)
		{
			tsp::Object a(1);
			a.data[0] = 13;
			tsp::Object b(a);
			Assert::IsTrue(13 == b.data[0]);
		}

		TEST_METHOD(Object_assign)
		{
			tsp::Object a(1),b,c(20);
			a.data[0] = 5;
			a = a;
			c = b = a;
			Assert::IsTrue((c.data[0] == 5) && (b.data[0] == 5));
		}

		TEST_METHOD(Object_allocate)
		{
			tsp::Object a(4);
			Assert::IsTrue(a.allocate(2));
		}

		TEST_METHOD(Object_isequal)
		{
			tsp::Object a(1), b(1),c(1);
			a.data[0] = 1;
			b.data[0] = 1;
			c.data[0] = 20;
			Assert::IsTrue((a == b) && !(b == c) && !(a == c));
		}
	};
}