#include "stdafx.h"
#include "CppUnitTest.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(UnitTestMap)
	{
	public:

		TEST_METHOD(Map_base)
		{
			tsp::Map_base a;
			Assert::IsTrue(a.getsize() == NULL);
		}

		TEST_METHOD(Map_load)
		{
			tsp::Map_base a;
			a.load("loadtest.txt");
			Assert::IsTrue((a.get({ 2,3 }) > 1.4141) && (a.get({ 2,3 }) < 1.4143));
		}

		TEST_METHOD(Map_loadtab)
		{
			tsp::Map_base a;
			a.loadtab("loadtabtest.txt");
			Assert::IsTrue((a.get({ 2,3 }) > 1.4141) && (a.get({ 2,3 }) < 1.4143));
		}
	};
}