#pragma once
#include "Object.h"
#include "Map_base.h"
#include <cstdlib>
#include <time.h>
/*
	Base for tsp solvers:
	- selecting (population) best object based on value
	- sort limited to finding best objects 
	- crossing unsing PAX
	- Limiting factor can be either time or number of cycless
*/
namespace tsp {
	struct settings
	{
		unsigned int population;
		unsigned int max_population;
		unsigned int max_iterations;
		double max_time;
	};

	class Solver_base
	{
	public:
		Solver_base();
		Solver_base(tsp::settings);
		Solver_base(tsp::settings, tsp::Map_base*);
		virtual ~Solver_base();
	protected:
		struct snode
		{
			Object* obj_ptr;
			mutable long unsigned int obj_value;
		};

		snode solution;
		snode* obj_list;

		//Full sort
		virtual void sort();

		//Sort limited to specified number of Objectswith lowest value
		virtual void sort(unsigned int size);

		tsp::settings settings;
		clock_t beginclock;
		clock_t endclock;
		unsigned int iterator;
		bool initialized;

		char * val1;
		char * val2;

		//No input control, Objects must be of same size or behavior will be undefined
		virtual void* combine(Object*, const Object*, const Object*);
		virtual void* mutate();
		virtual void* farmut();
		virtual void* mutate(Object*, const Object*);
		void calculate(const snode &);
		void newrand(snode *);
	public:
		tsp::Map_base * map;

		//Randomize initial population, settings must be set, map must be loaded or call will fail (safe)
		void initiate(unsigned int seed);
		void initiate();

		//Create new max_population
		virtual void populate();

		//Select new base population
		virtual void select();
		
		//Solve given problem with current settings
		virtual Object solve();

		//Population, max_population, max_iterations, max_time
		bool setsettings(tsp::settings);
		
		tsp::settings getsettings();
		unsigned int getiterator() const;
		double gettime() const;

		bool setmap(tsp::Map_base * mapbased_ptr);

		//Use to veryfy Object integrity
		bool veryfy(Object * target);

		//Get current solution
		Object getsolution() const;
		unsigned int getsolutionval()const;
	};

}