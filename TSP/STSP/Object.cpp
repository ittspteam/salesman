#include "Object.h"
tsp::Object::Object()
{
	datasize = NULL;
	data = nullptr;
}

tsp::Object::Object(unsigned int n)
{
	datasize = n;
	data = new char[datasize];
}

tsp::Object::Object(const Object& cpyme)
{
	datasize = cpyme.datasize;
	data = new char[datasize];
	memcpy(this->data, cpyme.data, datasize*sizeof(char));
}

tsp::Object::~Object()
{
	delete[] data;
	data = nullptr;
}

bool tsp::Object::allocate(unsigned int n)
{
	if (datasize != n)
	{
		delete[] data;
		datasize = n;
		data = new char[datasize];
	}
	if (!datasize) return false;
	return true;
}

/*
char* Object::get() const
{
	return data;
}

bool assign(char* newdata, unsigned int size)
{
	if (newdata == nullptr)return false;
	if (size) return false;
	memcpy(data, newdata, size);
	return true;
}
*/

bool tsp::Object::isequal(const Object* L_obj, const Object* R_obj) const
{
	return !std::memcmp(L_obj->data, R_obj->data,datasize * sizeof(char));
}

bool tsp::Object::assign(const Object R_obj)
{
	if (!this->allocate(R_obj.datasize)) return false;
	memcpy(this->data, R_obj.data, datasize * sizeof(char));
	return true;
}
