#include "Map_base.h"
tsp::Map_base::Map_base()
{
	cordtable = nullptr;
	distancetable = nullptr;
	size = NULL;
}

tsp::Map_base::~Map_base()
{
	delete[] cordtable;
	if (distancetable != nullptr) for (unsigned int i = 0; i<size; i++)
		delete[] distancetable[i];
	delete[] distancetable;
}

unsigned int tsp::Map_base::calculate() const
{
	allocate();
	for (unsigned int i = 0; i < size; i++)
		for (unsigned int j = 0; j < i + 1; j++)
		{
			distancetable[j][i] = sqrt(pow((cordtable[i].x - cordtable[j].x), 2) + pow((cordtable[i].y - cordtable[j].y), 2));
			distancetable[i][j] = distancetable[j][i];
		}
	return 0;
}

bool tsp::Map_base::allocate() const
{
	if (distancetable != nullptr || size == 0) return false;
	distancetable = new unsigned int*[size];
	for (unsigned int i = 0; i < size; i++)
		distancetable[i] = new unsigned int[size];
	return true;

}

unsigned int tsp::Map_base::load(const std::string filename)
{
	std::fstream in;
	in.open(filename, std::ios::in);
	if (!in.is_open()) return 0;
	in >> size;
	cordtable = new vector2u[size];
	for (unsigned int i = 0; i < size; i++)
	{
		in >> cordtable[i].x;
		in >> cordtable[i].y;
	}
	in.close();
	calculate();
	return size;
}

unsigned int tsp::Map_base::loadtab(const std::string filename)
{
	std::fstream in;
	in.open(filename, std::ios::in);
	if (!in.is_open()) return 0;
	in >> size;
	allocate();
	for (unsigned int i = 0; i < size; i++)
		for (unsigned int j = 0; j < i + 1; j++)
		{
			in >> distancetable[j][i];
			distancetable[i][j] = distancetable[j][i];
		}
	in.close();
	return size;
}

unsigned int tsp::Map_base::getsize() const
{
	return size;
}

unsigned int tsp::Map_base::get(vector2u pos) const
{
	return distancetable[pos.y][pos.x];
}