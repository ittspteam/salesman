#include "Solver_base.h"
#include <iostream>

tsp::Solver_base::Solver_base()
{
	solution.obj_ptr = nullptr;
	solution.obj_value = NULL;
	obj_list = nullptr;
	settings = { NULL, NULL, NULL, NULL };
	beginclock = NULL;
	endclock = NULL;
	iterator = 0;
	initialized = false;
	map = nullptr;
	val1 = nullptr;
	val2 = nullptr;
}

tsp::Solver_base::Solver_base(tsp::settings newsettings)
{
	solution.obj_ptr = nullptr;
	solution.obj_value = NULL;
	obj_list = nullptr;
	settings = newsettings;
	beginclock = NULL;
	endclock = NULL;
	iterator = 0;
	initialized = false;
	map = nullptr;
	val1 = nullptr;
	val2 = nullptr;
}

tsp::Solver_base::Solver_base(tsp::settings newsettings, tsp::Map_base * ptr)
	:Solver_base(newsettings)
{
	map = ptr;
}

tsp::Solver_base::~Solver_base()
{
	if(obj_list!=nullptr) for (unsigned int i = 0; i < settings.population; i++)
			delete obj_list[i].obj_ptr;
	delete []obj_list;
	delete[] val1;
	delete[] val2;
	//delete solution.obj_ptr;???
}

void tsp::Solver_base::sort()
{
	sort(settings.population);
}

void tsp::Solver_base::sort(unsigned int size)
{
	snode temp;
	for (unsigned int k = 0; k < size - 1; k++)
	{
		for (unsigned int i = settings.max_population-1-k; i > 0; i--)
		{
			while (obj_list[i - 1].obj_value == obj_list[i].obj_value) newrand(&obj_list[i]);
			if ( obj_list[i - 1].obj_value > obj_list[i].obj_value)
			{
				temp = obj_list[i];
				obj_list[i] = obj_list[i - 1];
				obj_list[i - 1] = temp;
			}
		}
	}
}

void* tsp::Solver_base::combine(Object * Target,const Object * L_obj, const Object *R_obj)
{ 
	unsigned int size = R_obj->datasize;
	unsigned int cut = ((size-1) - (size-1) % 3) / 3;	
	
	//veryfy((Object*)L_obj);////
	//veryfy((Object*)R_obj);////

	for (unsigned int i = 1; i < cut+1; i++)
	{
		Target->data[i] = R_obj->data[i];
		Target->data[cut*2 + i] = R_obj->data[cut*2 + i];
	}
	
	for (unsigned int i = cut * 3 + 1; i < size; i++)//Surplus: OK
		Target->data[i] = R_obj->data[i];

	for (unsigned int i = 0; i < cut; i++)//Val tables: OK
	{
		val2[i] = L_obj->data[cut + 1 + i];
		val1[i] = R_obj->data[cut + 1 + i];
		Target->data[cut + 1 + i] = L_obj->data[cut + 1 + i];
	}
	for (unsigned int i = 0; i < cut; i++)
	{
		for (unsigned int j = 0; j < cut; j++)
		{
			if (j == i) if(val1[i] == val2[j])break; else continue;
			if (val1[i] == val2[j])
			{
				val2[i] = val2[i] + val2[j];
				val2[j] = val2[i] - val2[j];
				val2[i] = val2[i] - val2[j];
				break;
			}
		}
	}

	//This will go out of scope if val1-val2 is incorrect or base objects are incorrect
	for (unsigned int i = 0, j = 1; i < cut; i++, j = 1)
	{
		if (val1[i] == val2[i]) continue;
		while (Target->data[j] != val2[i])
		{
			j++;
			if (j == cut+1)j = 2 * cut+1;		//This line seems to work well
			//if (j == size) __debugbreak();////  //Entry objects are OK, No invalid objects were created before
		}
		Target->data[j] = val1[i];
	}
	//veryfy(Target);////
	return (void*)Target;
}

void * tsp::Solver_base::mutate()
{
	unsigned int i;
	snode mutated;
	mutated.obj_ptr = new Object(*obj_list[0].obj_ptr);
	i = rand() % (obj_list[0].obj_ptr->datasize - 2) + 1;
	mutated.obj_ptr->data[i] = obj_list[0].obj_ptr->data[i + 1];
	mutated.obj_ptr->data[i + 1] = obj_list[0].obj_ptr->data[i];
	calculate(mutated);
	if (mutated.obj_value < obj_list[0].obj_value)
	{
		obj_list[0].obj_ptr->data[i] = mutated.obj_ptr->data[i];
		obj_list[0].obj_ptr->data[i+1] = mutated.obj_ptr->data[i+1];
		obj_list[0].obj_value = mutated.obj_value;
	}
	delete mutated.obj_ptr;
	return nullptr;
}

void * tsp::Solver_base::farmut()
{
	unsigned int a,b;
	snode mutated;
	mutated.obj_ptr = new Object(*obj_list[0].obj_ptr);
	a = rand() % (obj_list[0].obj_ptr->datasize - 2) + 1;
	while (a == (b = rand() % (obj_list[0].obj_ptr->datasize - 2) + 1));

	mutated.obj_ptr->data[a] = obj_list[0].obj_ptr->data[b];
	mutated.obj_ptr->data[b] = obj_list[0].obj_ptr->data[a];
	calculate(mutated);
	if (mutated.obj_value < obj_list[0].obj_value)
	{
		obj_list[0].obj_ptr->data[a] = mutated.obj_ptr->data[a];
		obj_list[0].obj_ptr->data[b] = mutated.obj_ptr->data[b];
		obj_list[0].obj_value = mutated.obj_value;
	}
	delete mutated.obj_ptr;
	return nullptr;
}

//TODO: Optimalize me
void * tsp::Solver_base::mutate(Object * Target, const Object * source)
{
	unsigned int i;
	char temp;
	i = rand() % (source->datasize - 2) + 1;
	*Target = *source;
	temp = source->data[i];
	Target->data[i]= source->data[i+1];
	Target->data[i+1] = temp;
	return nullptr;
}

void tsp::Solver_base::calculate(const snode &a)
{
	a.obj_value = 0;
	unsigned int size = a.obj_ptr->datasize;
	for (unsigned int i = 0; i < size-1; i++)
		a.obj_value += map->get({ (unsigned int)a.obj_ptr->data[i],(unsigned int)a.obj_ptr->data[i+1] });
	a.obj_value += map->get({ (unsigned int)a.obj_ptr->data[0], (unsigned int)a.obj_ptr->data[size - 1] });
}

void tsp::Solver_base::newrand(snode * ptr)
{
	unsigned int map_size = ptr->obj_ptr->datasize;
	unsigned int random;
	char temp;
	for (unsigned int k = 0; k < map_size; k++)
		ptr->obj_ptr->data[k] = k;
	for (unsigned int k = 1; k < map_size; k++)
	{
		random = rand() % (map_size - 1) + 1;
		temp = ptr->obj_ptr->data[k];
		ptr->obj_ptr->data[k] = ptr->obj_ptr->data[random];
		ptr->obj_ptr->data[random] = temp;
	}
	//veryfy(ptr->obj_ptr);////
	calculate(*ptr);
}

//TODO: Add maxpopulation control
void tsp::Solver_base::initiate(unsigned int seed)
{
	if (settings.population == 0 || settings.max_population <= settings.population || map == nullptr) return;
	unsigned int map_size = map->getsize();
	if (map_size == 0) return;

	std::srand(seed);
	solution.obj_ptr = new Object(map_size);
	obj_list = new snode[settings.max_population];

	char temp;
	unsigned int random;

	for (unsigned int i=0;i<settings.population;i++)
	{
		obj_list[i].obj_ptr = new Object(map_size);
		for (unsigned int k = 0; k < map_size; k++)
			obj_list[i].obj_ptr->data[k] = k;
		for (unsigned int k = 1; k < map_size; k++)
		{
			random = rand() % (map_size - 1) + 1;
			temp = obj_list[i].obj_ptr->data[k];
			obj_list[i].obj_ptr->data[k] = obj_list[i].obj_ptr->data[random];
			obj_list[i].obj_ptr->data[random] = temp;
		}
		//veryfy(obj_list[i].obj_ptr);////
		calculate(obj_list[i]);
	}
	solution = obj_list[0];
	for (unsigned int i = settings.population; i < settings.max_population; i++)
	{
		obj_list[i].obj_ptr = new Object(map_size);
		obj_list[i].obj_ptr->data[0] = 0;
		obj_list[i].obj_value = NULL;
	}

	unsigned int a = ((map->getsize()-1) - (map->getsize()-1) % 3) / 3;
	if (val1 != nullptr) delete[] val1;
	if (val2 != nullptr) delete[] val2;
	val1 = new char[a];
	val2 = new char[a];
	initialized = true;
}

void tsp::Solver_base::initiate()
{
	initiate((unsigned int)time(NULL));
}

void tsp::Solver_base::populate()
{
	/*
	unsigned int i = settings.population, k = 0, f = settings.max_population;
	while (i < f)
	{
		combine(obj_list[i].obj_ptr, obj_list[k].obj_ptr, obj_list[k+1].obj_ptr);
		calculate(obj_list[i]);
		k += 2; i++;
		if (k > settings.population)break;
	}
	while (i < f)
	{
		combine(obj_list[i].obj_ptr, obj_list[k].obj_ptr, obj_list[k + 1].obj_ptr);
		calculate(obj_list[i]);
		k -= 2; i++;
		if (k == 0)break;
	}
	k++;
	while (i < f)
	{
		combine(obj_list[i].obj_ptr, obj_list[k].obj_ptr, obj_list[k + 1].obj_ptr);
		calculate(obj_list[i]);
		k += 2; i++;
		if (k == settings.population+1)break;
	}
	while (i < f)
	{
		combine(obj_list[i].obj_ptr, obj_list[k].obj_ptr, obj_list[k + 1].obj_ptr);
		calculate(obj_list[i]);
		k -= 2; i++;
		if (k == 1)break;
	}
	while (i < f)
	{
		newrand(&obj_list[i]);
		i++;
	}
	f = settings.population;
	for (i = 1; i < f; i+=2) newrand(&obj_list[i]);*/
	unsigned int i = settings.population, j = 0, k = 1;
	while (i < settings.max_population )
	{
		combine(obj_list[i].obj_ptr, obj_list[j].obj_ptr, obj_list[k].obj_ptr);
		calculate(obj_list[i]);
		i++; k++;
		if (k == j) k++;
		if (k == settings.population) { k = 0; j++; }
	}
}
int stag = 3;
void tsp::Solver_base::select()
{
	farmut();
	farmut();
	sort();
			
	if (obj_list[0].obj_value < solution.obj_value)
	{
		solution.obj_ptr = obj_list[0].obj_ptr;
		solution.obj_value = obj_list[0].obj_value;
		//std::cout << iterator << ". \t" << solution.obj_value << std::endl;
		for (int i = 0; i < 20; i++) farmut();
		while (solution.obj_value > obj_list[0].obj_value)
		{
			solution.obj_ptr = obj_list[0].obj_ptr;
			solution.obj_value = obj_list[0].obj_value;
			//std::cout << iterator << ". \t" << solution.obj_value << std::endl;
			for(int i = 0; i < stag;i++) farmut();
		}
	}
	if (stag < 100000)stag++;
	
	else
	newrand(& obj_list[1]);
		
}

tsp::Object tsp::Solver_base::solve()
{
	if (!initialized) initiate();
	if (!initialized) return Object();
	beginclock = clock();
	while (iterator<settings.max_iterations)
	{
		populate();
		select();
		iterator++;
		if (gettime() >= settings.max_time) return *solution.obj_ptr;
	}
	return *solution.obj_ptr;
}

tsp::settings tsp::Solver_base::getsettings()
{
	return settings;
}

bool tsp::Solver_base::setsettings(tsp::settings newsettings)
{
	settings = newsettings;
	return true;
}

unsigned int tsp::Solver_base::getiterator() const
{
	return iterator;
}

double tsp::Solver_base::gettime() const
{
	if (endclock == NULL) return (double)(clock() - beginclock) / CLOCKS_PER_SEC;
	return (double)(endclock - beginclock) / CLOCKS_PER_SEC;
}

bool tsp::Solver_base::setmap(tsp::Map_base * mapbased_ptr)
{
	if (mapbased_ptr == nullptr) return false;
	map = mapbased_ptr;
	return true;
}

bool tsp::Solver_base::veryfy(Object * target)
{
	unsigned int controlsum = 0;
	unsigned int n = target->datasize;
	for (unsigned int i = 0; i < n; i++)
		controlsum += target->data[i];
	if (controlsum != (unsigned int)((pow(n, 2) - n) / 2.0))
	{
		__debugbreak();
		return true;
	}
	return false;
}

tsp::Object tsp::Solver_base::getsolution() const
{
	if (solution.obj_ptr == nullptr) return Object();
	return Object(*solution.obj_ptr);
}

unsigned int tsp::Solver_base::getsolutionval() const
{
	return solution.obj_value;
}
