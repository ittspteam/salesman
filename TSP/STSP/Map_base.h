#pragma once
#include <string>
#include <fstream>
namespace tsp {
	template <typename type> struct vector2
	{
		type x;
		type y;
	};

	typedef vector2<double>			vector2d;
	typedef vector2<int>			vector2i;
	typedef vector2<unsigned int>	vector2u;

	class Map_base
	{
	public:
		Map_base();
		virtual ~Map_base();
	protected:
		vector2u* cordtable;
		mutable unsigned int** distancetable;
		unsigned int calculate() const;
		unsigned int size;
		bool allocate() const;
	public:
		unsigned int load(const std::string filename);
		virtual unsigned int loadtab(const std::string filename);
		unsigned int getsize() const;
		unsigned int get(vector2u pos) const;
	};

}