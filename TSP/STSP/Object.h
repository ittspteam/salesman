#pragma once
#include <cstring>
//TODO: Add [] overload
namespace tsp {
	class Object
	{
		bool isequal(const Object* L_obj, const Object* R_obj) const;
		bool assign(const Object R_obj);
	public:
		Object();
		Object(unsigned int size);
		Object(const Object&);
		~Object();

		//Protected or private data would mean loss in performance
		char* data;

		//Make me static?
		unsigned int datasize;

		/*
		//Not needed if data isnt protected
		char* get() const;
		bool assign(char*,unsigned int);
		*/

		bool allocate(unsigned int datasize);

		bool operator == (const Object& R_obj) const
		{
			return isequal(this, &R_obj);
		}

		Object& operator = (const Object& R_obj)
		{
			if (this == &R_obj) return *this;
			this->assign(R_obj);
			return *this;
		}
	};
}

