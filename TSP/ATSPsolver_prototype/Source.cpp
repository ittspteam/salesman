#include "AMap.h"
#include "Solver_base.h"
#include <iostream>
#include <iomanip>

#define BAD_INI "Initialization failed"
#define NO_ITER "No iterations occured"
#define NO_MAP  "Map not loded"
#define NO_TIME "Not enought time allowed"

int main(int argc,char* argv[])
{
	int err;
	std::cout << "Solver running" << std::endl;
	tsp::Map_base *test_map = new tsp::AMap;
	tsp::Solver_base test_solver;
	std::string solution;

	std::cout << "Loading" << std::endl;
	if (argc > 1)
	{
		test_map->loadtab(argv[1]);
		std::cout << "Ok" << std::endl;
	}
	else
	{
		std::cout << "Error too few arguments" << std::endl;
		test_map->loadtab("nomap.txt");
	}
	if (test_map->getsize() < 4)
	{
		std::fstream out;
		out.open("solution.txt", std::ios::out);
		std::cout << std::endl << "Solution: ";
		if (!out.is_open()) std::cout << "Error opening file" << std::endl;
		for (unsigned int i = 0; i < test_map->getsize(); i++)
		{
			std::cout << i << " ";
			out << i << " ";
		}
		return 1;
	}
	std::cout << "Solver starting" << std::endl;
	test_solver.setmap(test_map);
	test_solver.setsettings({ 30,90,500000,1.0 });
	test_solver.solve();

	//Solve fail interpreter 
	if (test_solver.getsolution().data == nullptr)
	{

		std::cout << "Error solving case: ";
		if (test_solver.getiterator() == 0) { std::cout << BAD_INI << std::endl; err = 101; }
		if (test_solver.map == nullptr) { std::cout << NO_MAP << std::endl; err = 404; }
		if (test_solver.getsettings().max_iterations == 0) { std::cout << NO_ITER << std::endl; err = 102; }
		else if (test_solver.getsettings().max_time < 0) { std::cout << NO_TIME << std::endl; err = 103; }
		system("pause");
		return err;
	}
	if (test_solver.veryfy(&test_solver.getsolution())) std::cout << "Invalid Solution " << std::endl;
	//Display-converter module
	
	unsigned int size = test_solver.getsolution().datasize;
	int val = 0;
	std::fstream out;
	out.open("solution.txt", std::ios::out);
	std::cout << std::endl << "Solution: ";
	if (!out.is_open()) std::cout << "Error opening file";
	else
	for (unsigned int i = 0; i < size; i++)
	{
		val = test_solver.getsolution().data[i];
		out << val << " ";
	}
	if (out.is_open())out.close();

	for (unsigned int i = 0; i < size; i++)
	{
		val = test_solver.getsolution().data[i];
		std::cout << val << " ";
	}
	
	std::cout << std::endl;
	std::cout << "elapsed time: " << test_solver.gettime() << std::endl;
	std::cout << "iterations: " << test_solver.getiterator() << std::endl << std::endl;
	delete(test_map);

	std::cout << "Done" << std::endl;
	return 0;
}
