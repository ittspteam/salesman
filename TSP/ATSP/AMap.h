#pragma once
#include "Map_base.h"
namespace tsp {
	class AMap
		:public tsp::Map_base
	{
	public:
		AMap();
		~AMap();
		virtual unsigned int loadtab(const std::string filename) override;
	};
}