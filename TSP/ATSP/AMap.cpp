#include "AMap.h"

tsp::AMap::AMap()
	:Map_base()
{

}

tsp::AMap::~AMap()
{

}

unsigned int tsp::AMap::loadtab(const std::string filename) 
{
	std::fstream in;
	in.open(filename, std::ios::in);
	if (!in.is_open()) return 0;
	in >> size;
	allocate();
	for (unsigned int i = 0; i < size; i++)
		for (unsigned int j = 0; j < size ; j++)
		{
			in >> distancetable[j][i];
		}
	in.close();
	return size;
}
